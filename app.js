'use strict';

class Gol {

  constructor(rows, columns, initialCount, debuggingMessages, initialValues){
    this.columns = columns;
    this.rows = rows;
    this.initialCount = initialCount;
    this.debuggingMessages = !!debuggingMessages;
    this.initialValues = initialValues;


    this.gameArr = [];

    for(let i = 0 ; (i + 1) <= this.rows * this.columns ; i++) {
      this.gameArr.push(0);
    }

    this.gameLength = this.gameArr.length;

  }


  start() {
    this.initialize(this.initialCount);
  }
  
  initialize(number) {
    let that = this;

    function createOrganism() {
      let randomPos = Math.floor(Math.random() * that.gameLength);

      if(!that.gameArr[randomPos]) {
        that.gameArr[randomPos] = 1;
      }
      else{
        createOrganism();
      }
    }

    if(that.initialValues){
      that.gameArr = that.initialValues;
    }else{
      for( let i = 0 ; i < number ; i++  ) {
        createOrganism();
      }
    }

    if(that.debuggingMessages) {
      that.presentation(that.gameArr);
    }
    
    that.checkings();
      
  }


  checkings() {
    
    let that = this;

    const elWebWorker = new Worker('web_worker.js');

    elWebWorker.postMessage({arr: that.gameArr, columns: that.columns, rows: that.rows, debuggingMessages: that.debuggingMessages});
    
    elWebWorker.onmessage = (response => {
      that.presentation(response.data);
      elWebWorker.postMessage({arr: response.data, columns: that.columns, rows: that.rows, debuggingMessages: that.debuggingMessages});
    });

    



  }



  presentation(arr) {
    let that = this;
    let board = [];
    let workBoard = arr.slice();

    for(var r = 0 ; r < that.rows ; r++){
      board[r] = workBoard.splice(0, that.columns);
      console.log(board[r]);

    }
    console.log("%c😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊 😊", "color: #FFFFFF; background: #303F9F; font-size: 1.5em");
    console.log("%c                  New generation                      ", "color: #FFFFFF; background: #3949AB; font-size: 1.5em");
  }

    




}


//let initialValues = [0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

//let gol = new Gol(5, 6, 25, false, initialValues);

let gol = new Gol(20, 20, 200);


gol.start();
