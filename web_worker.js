'use strict';


self.onmessage = function(config) {
  self.gameArr = config.data.arr;
  self.columns = config.data.columns;
  self.rows = config.data.rows;
  self.debuggingMessages = config.data.debuggingMessages;

  check();
}



function check(){

  self.nextArr = [];
  self.gameArr.map((elem, index, arr) => {

    let neighbors = 0;


    // Top

    if(index >= self.columns) {

      if(arr[(index - self.columns) - 1]) {
        neighbors ++;
      }

      if(arr[(index - self.columns)]) {
        neighbors ++;
      }
    
      if(arr[(index - self.columns) + 1]) {
        neighbors ++;
      }

    }




    // Left
    if(index % self.columns !== 0 && index !== 0) {
      if(arr[index - 1]) {
        neighbors ++;
      }
    }
    

    // Right
    if((index + 1) % self.columns !== 0 || index === 0) {
      if(arr[index + 1]) {
        neighbors ++;
      }
    }

    // Bottom

    if(index < ((self.columns * self.rows) - self.columns)) {
      // Bottom right
      if(index % self.columns !== 0) {
        if(arr[(index + self.columns) - 1]) {
          neighbors ++;
        }
      }

      // Bottom middle
      
        if(arr[(index + self.columns)]) {
          neighbors ++;
        }
      

      // Bottom left
      if((index + 1) % self.columns !== 0) {
        if(arr[(index + self.columns) + 1]) {
          neighbors ++;
        }
      }

    } 
    
    if(self.debuggingMessages) {
      console.log(`elem in pos ${index} has ${neighbors} neighbors`);
    }

    if(elem){

      if(neighbors < 2) {
        if(self.debuggingMessages) {
          console.log('I died');
        }

        nextArr[index] = 0;
      }else if(neighbors > 3) {
        if(self.debuggingMessages) {
          console.log('Too many people dancing around me, I\'m out');
        }

        nextArr[index] = 0;
      }else{
        if(self.debuggingMessages) {
          console.log('I live to see another day');
        }

        nextArr[index] = elem;
      }

    }else{

      if(neighbors === 3) {
        if(self.debuggingMessages) {
          console.log("%cHello world!", "color: #FFFFFF; background: #303F9F;");
        }

        nextArr[index] = 1;
      }else{

        nextArr[index] = elem;
      }

    }



  })

  self.postMessage(self.nextArr);
}



