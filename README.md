## Wutt?

Open the console (F12).

This is a Conway's game of life implementation that uses a [web worker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers) to handle most of the logic. In this implementation I used a single array of data as source.

You will see/feel that the web worker makes the app really performant, I'm running the app with a 50x50 matrix in an old Intel Core i3 quad processor with lots of heavy web apps in parallel and runs [just fine](http://i1.kym-cdn.com/entries/icons/original/000/019/698/d96.jpg).

The client (app.js) sends a message to the web worker with the array of data that needs to be processed, when finised the worker sends a new array. Finally the client shows the result array (split into as many rows as specified initially) in the console and sends that same array back to the worker, the process repeats indefinitely.

You will see an array with 1 and 0, 1 means there is a living organism and 0 means it is dead.


## Usage

`Gol` is the main class, you need to instantiate it and pass the following parameters:

- rows                   Number. The rows the matrix/'game board' should have
- columns                Number. The rows the matrix/'game board' should have
- initialCount           Number. The amount of organisms that should be present at the beginning
- debuggingMessages      Boolean. Optional. Should the app show debugging console logs
- initialValues          Array. Optional. A custom array including a custom game board specification (remember, 1: alive, 0: dead)

Then just call `.start()`

## Sample

```
let gol = new Gol(20, 20, 200);


gol.start();

```

## Things missing

Unit tests :/